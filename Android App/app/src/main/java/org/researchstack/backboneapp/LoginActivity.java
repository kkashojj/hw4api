package org.researchstack.backboneapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class LoginActivity extends AppCompatActivity {
    EditText edusername,edpassword;
    String username, password;
    Button btnlogin;
    SharedPreferences sharedpref;
    static String previoussessiontoken;
    ApiStatus api_response;
    static final String API_URL = "http://ec2-34-209-120-112.us-west-2.compute.amazonaws.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewdByIDs();
        previoussessiontoken = getPreviousSessionTokenFronSharedPreferences();
        if(previoussessiontoken!=null){
            goToSurveyActivity();
        }

    }

    private void goToSurveyActivity() {
        Intent intnt = new Intent(LoginActivity.this, SurveyActivity.class);
        startActivity(intnt);
    }

    private void findViewdByIDs() {
        edusername = (EditText) findViewById(R.id.ed_username);
        edpassword = (EditText) findViewById(R.id.ed_password);
        btnlogin = (Button) findViewById(R.id.btn_login);

    }
    public void loginUser(View view) {
        assignValuesToVariables();
        if(username!=null && password!=null && username.length()!=0 && password.length()!=0) {
            loginUserToServer(new User(username,password));
        }
        else{
            displayToast("Please enter all the values to proceed further..");
        }
    }

    private void assignValuesToVariables(){
        username = edusername.getText().toString();
        password = edpassword.getText().toString();
    }

    public String getPreviousSessionTokenFronSharedPreferences() {
        sharedpref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String token = sharedpref.getString("MyPreviousSession","");
        if(token!=null && token.length()>=1 ){
            return token;
        }
        return null;
    }


    private void displayToast(String s) {
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

    private void loginUserToServer(User user) {
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "username="+user.username.toString()
                +"&password="+user.password.toString());
        Request request = new Request.Builder()
                .url(API_URL+"login")
                .post(body)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    try {
                        api_response = JsonParser.ParseGenericStatusObject.doJsonParsing(response.body().string());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LoginActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (api_response != null){
                                if(api_response.getSuccess()) {
                                displayToast("Login Successful!!");
                                sharedpref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                SharedPreferences.Editor editor = sharedpref.edit();
                                editor.putString("MyPreviousSession", api_response.getTokenid().toString());
                                editor.putString("previoususername", api_response.getClientid().toString());
                                editor.commit();
                                goToSurveyActivity();

                            } else {
                                displayToast("Login Failed." + api_response.getStatus().toString());
                            }
                        }
                            else {
                                displayToast("Login Failed.");
                            }
                        }
                    });

                }
            }
        });
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

