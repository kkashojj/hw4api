package org.researchstack.backboneapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Rama Vamshi Krishna on 08/30/2017.
 */
public class JsonParser {


    static class ParseGenericStatusObject{
        static ApiStatus doJsonParsing(String inputstring) throws JSONException {
            JSONObject rootobj = new JSONObject(inputstring);
            ApiStatus resobj = new ApiStatus();
            resobj.setStatus(rootobj.optString("status"));
            resobj.setSuccess(rootobj.optBoolean("success"));
            resobj.setTokenid(rootobj.optString("token"));
            resobj.setClientid(rootobj.optString("username"));
            return resobj;
        }
    }
    static class ParseQuestionsList {
        static ArrayList<Question> doJsonParsing(String inputstring) throws JSONException {
            ArrayList<Question> questions_list = new ArrayList<Question>();
            JSONObject rootobj = new JSONObject(inputstring);
            JSONArray qarray = rootobj.getJSONArray("questions");
            if (qarray!=null && qarray.length() > 0) {
                for (int i = 0; i < qarray.length(); i++) {
                    Question qobj = new Question();
                    JSONObject queobj = qarray.getJSONObject(i);
                    qobj.setId(queobj.optString("_id"));
                    qobj.setTitle(queobj.optString("text"));
                    qobj.setSection(queobj.optString("section"));
                    questions_list.add(qobj);
                }
                return questions_list;
            }
            return null;
        }
    }

}

