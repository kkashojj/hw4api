package org.researchstack.backboneapp;

/**
 * Created by Rama Vamshi Krishna on 11/08/2017.
 */

public class Answer {

    String qid, ans;

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public Answer(String qid, String ans) {

        this.qid = qid;
        this.ans = ans;
    }
}
