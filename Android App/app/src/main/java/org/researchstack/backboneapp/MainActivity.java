package org.researchstack.backboneapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;
    SharedPreferences sharedpref;
    static String previoussessiontoken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView(R.layout.activity_main);
        showSplashScreenAndMoveOn();
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void showSplashScreenAndMoveOn() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                previoussessiontoken = getPreviousSessionTokenFronSharedPreferences();
                if(previoussessiontoken!=null){
                    goToSurveyActivity();
                }
                else {
                    goToLoginActivity();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    public String getPreviousSessionTokenFronSharedPreferences() {
        sharedpref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String token = sharedpref.getString("MyPreviousSession","");
        if(token!=null && token.length()>=1 ){
            return token;
        }
        return null;
    }

    private void goToSurveyActivity() {
        Intent intnt = new Intent(MainActivity.this, SurveyActivity.class);
        startActivity(intnt);
        finish();
    }
    private void goToLoginActivity() {
        Intent intnt = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intnt);
        finish();
    }
}
