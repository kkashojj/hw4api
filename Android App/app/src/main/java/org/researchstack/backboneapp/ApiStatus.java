package org.researchstack.backboneapp;

/**
 * Created by Rama Vamshi Krishna on 08/30/2017.
 */
public class ApiStatus {
    String status,clientid,tokenid;
    Boolean success;

    public ApiStatus() {
    }

    public ApiStatus(String status, Boolean success) {
        this.status = status;
        this.success = success;
    }

    public ApiStatus(String clientid, String status, Boolean success, String tokenid) {
        this.clientid = clientid;
        this.status = status;
        this.success = success;
        this.tokenid = tokenid;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getTokenid() {
        return tokenid;
    }

    public void setTokenid(String tokenid) {
        this.tokenid = tokenid;
    }
}
