import express from 'express';
let router = express.Router();
import dbhandlers from './handlers/dbHandlers';
import circular from 'circular-json';
import jwt from 'jsonwebtoken';
import config from './config';
import regionHandlers from './handlers/regionHandlers';
import messageHandlers from './handlers/messageHandlers';
import bodyParser from 'body-parser';
import mysession from "./model/mysession";

router.use((req,res,next)=>{
    if(req.path===("/login")){
        next();
    }else{
        let token = req.header.token|| req.header('token');
        let username = req.header.username|| req.header('username');

        if(token){
            jwt.verify(token,config.secret,(err,decoded)=>{
                if(err)
                    return res.status(400).json({success:false,message:"failed to authenticate"});
                else{
                    console.log(decoded);
                    next();
                }
            });
        }
        else
            return res.status(400).json({success:false,message:"token needed"});
    }
});

router.get('/',(req,res)=>{
  res.send(circular.stringify(req));
});

router.post("/addUser",dbhandlers.addUser);

router.post("/login",dbhandlers.login);

router.delete('/allQuestions',dbhandlers.deleteAllQuestions);

router.get("/logout",dbhandlers.logout);

router.get("/valid",dbhandlers.validate);

router.get("/users",dbhandlers.allUsers);

router.delete("/users",dbhandlers.deleteAllUsers);

router.post("/addQuestion",dbhandlers.addQuestion);

router.get("/showQuestions",dbhandlers.showQuestion);

router.delete("/deleteQuestion",dbhandlers.deleteQuestion);

router.post("/submitAnswers",dbhandlers.submitAnswers);

router.get("/responses",dbhandlers.responses);

router.get("/getResponse",dbhandlers.getResponse);

router.delete("/response",dbhandlers.deleteResponse);

router.get("/logout",dbhandlers.logout);

// router.get("/products",regionHandlers.productFinder);
//
// router.get("/productImage",regionHandlers.imageFinder);
//
// router.get("/productsLoad",regionHandlers.productLoad);//dont let it work


export default router;
