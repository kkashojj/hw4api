import mongoose from 'mongoose';


const product = new mongoose.Schema({
  'discount': {
      'type': Number,
          'required': true
  },
  'name': {
      'type': String,
          'unique': true,
          'required': true
  },
  'photo': {
      'type': String
  },
  'price': {
          'type': Number,
          'required': true
  },
  'region': {
          'type': String,
          'required': true
  }
});

export default mongoose.model('product', product);
