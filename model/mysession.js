import mongoose from 'mongoose';

const mysession = new mongoose.Schema({
    'username': {
        'type': String,
        'required': true
    },
    'token': {
        'type': String,
        'required': true
    }
});

export default mongoose.model('MySession2', mysession);
