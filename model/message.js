import mongoose from 'mongoose';


const message = new mongoose.Schema({
  'mid': {
      'type': String,
      'required': true,
      'unique': true
  },
  'sender': {
      'type': String,
      'required': true
  },
  'receiver': {
      'type': String,
      'required': true
  },
  'time': {
          'type': String,
          'required': true,
          'default': Date.now('milli')
  },
  'region': {
          'type': String,
          'required': true
  },
  'rstatus':{
          'type': Boolean,
          'required': true,
          'default': false
  },
  'lstatus':{
          'type': Boolean,
          'required': true,
          'default': false
  },
  'desc':{
        'type': String
  }
});

export default mongoose.model('message', message);
