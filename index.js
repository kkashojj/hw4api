import http from 'http';
import express from 'express';
var app = express();
import mongoose from 'mongoose';
import config from './config';
import router from './routes';
import bodyParser from 'body-parser';
import cors from 'cors';

mongoose.connect(config.db.uri, {}, (err)=> {
  if (err) {
    console.log('Connection Error: ', err);
  } else {
    console.log('Successfully Connected to db');
  }
});

app.use(cors())
// app.use(function (req, res, next) {
//
//     // Website you wish to allow to connect
//     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
//
//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//
//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,username,token');
//
//     // Set to true if you need the website to include cookies in the requests sent
//     // to the API (e.g. in case you use sessions)
//     res.setHeader('Access-Control-Allow-Credentials', true);
//
//     // Pass to next layer of middleware
//     next();
// });
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use("/",router);

app.listen(process.env.PORT||config.port,()=>{
  console.log("connected to port "+config.port);
})




console.log('Server running at http://127.0.0.1:y1337/');
