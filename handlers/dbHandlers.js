import user from '../model/user';
import question from '../model/question';
import mysession from '../model/mysession';
import response from '../model/response';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import config from '../config';
var tm = require('token-manager');
var tokenManager = new tm.TokenManager();
let uuid= require('uuid/v4');


let handle = {

    login(req,res){
        let username = req.body.username;
        let password = req.body.password;
        // let name,username,password;
        console.log(username);
        user.findOne({'username':username},(err,User)=>{
            if(err){
                console.log(err);
            }else if(User){
                if(password===User.password){
                    var tokenGen=jwt.sign(User,config.secret);
                    var newSession=mysession({
                        username: username,
                        token:tokenGen
                    });
                    newSession.save((err,session)=>{
                        if(err){
                            console.log(err);
                            return res.status(400).json({status:'Session couldn\'t be established'});
                        }else{
                            res.status(200).json({success:true,status:'Login Successful',token:tokenGen, username : username});
                        }
                    });

                }else{
                    console.log(User);
                    return res.status(200).json({success:false,status:'You have entered wrong credentials'});
                }
            }else{
                return res.status(200).json({success:false,status:'User doesn\'t exists'})
            }
        })
    },
    logout(req,res){
        let logouttokenid = req.query.token;
        try {
            var tokentoberemoved = tokenManager.get(logouttokenid);
            tokentoberemoved.expire();
        }
        catch(nex){
            return res.status(400).json({success:false,status:'User Session not valid'});
        }
        return res.status(200).json({success:false,status:'User Session Expired'});
    },
    validate(req,res){
        let usertokenid = req.header.token|| req.header('token');
        jwt.verify(usertokenid,config.secret,(err,decoded)=>{
            if(err)
                return res.status(400).json({success:false,message:"failed to authenticate"});
            else{

                return res.status(200).json(
                    {
                        success:true,
                        status:'User Session valid',
                        tokenid:usertokenid,
                        userData:decoded
                    }
                );
            }
        });
    },
    allUsers(req,res)
    {
        user.find({},(err,users)=>
        {
            if(err)
            {
                res.status(400).json({success:false,status:'all user data failed'});
            } else
            {
                res.status(200).send(users);
            }
        })
    },
    deleteAllUsers(req,res){
        user.remove({},(err,data)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }
            else{
                res.status(200).json({success:true,status:'all user deleted'});
            }
        })
    },
    deleteAllQuestions(req,res){
        let username = req.header.username|| req.header('username');
        user.findOne({'username':username},(err,user)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }else if(user){
                if(user.role==='admin'){
                    question.remove({},(err,data)=>{
                        if(err){
                            res.status(400).json({success:false,status:'all question deletion failed'});
                        }
                        else{
                            res.status(200).json({success:true,status:'all questions deleted'});
                        }
                    })
                }else{
                    res.status(200).json({success:false,status:"Insufficient permissions"})
                }
            }
        });
    },deleteQuestion(req,res){
        let username = req.header.username|| req.header('username');
        let id=req.body.id;
        user.findOne({'username':username},(err,user)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }else if(user){
                if(user.role==='admin'){
                    question.remove({'_id':id},(err,data)=>{
                        if(err){
                            res.status(400).json({success:false,status:'all question deletion failed'});
                        }
                        else{
                            res.status(200).json({success:true,status:'Question deleted'});
                        }
                    })
                }else{
                    res.status(200).json({success:false,status:"Insufficient permissions"})
                }
            }
        });
    },
    addQuestion(req,res){
        let questionText=req.body.question;
        let section=req.body.section;
        let username = req.header.username|| req.header('username');
        let type=req.body.type;
        //let options=JSON.parse(req.body.options);

        console.log(questionText);
        user.findOne({'username':username},(err,usr)=>{
            if(err){

            }else if(usr){
                if(usr.role==='admin' || usr.role==='study'){
                    let que=new question({
                        'qid':uuid(),
                        'text':questionText,
                        'section':section,
                        'type':type
                        //'options':options
                    });
                    que.save((err,ques)=>{
                       if(err){
                           return res.status(400).json({success:false,status:'Error!! Please Try Again',error:err.toString()});
                       } else if(ques){
                           return res.status(400).json({success:true,status:'Question added successfully',Question:ques});
                       }else{
                           return res.status(400).json({success:false,status:'Error!! Please Try Again',error:"Question not saved to db"});
                       }
                    });
                }else{
                    return res.status(200).json({success:false,status:'Onlu admin can add questions',error:"Question not saved to db"});
                }
            }
        });
    },
    showQuestion(req,res){
        question.find({},(err,ques)=>{
            if(err){
                return res.status(400).json({success:false,status:'Error!! Please Try Again'});
            }else if(ques){
                return res.status(200).json({success:true,status:'Successful',questions:ques});
            }else{
                return res.status(400).json({success:false,status:'Error!! Please Try Again'});
            }
        });
    },
    submitAnswers(req,res){
        let resps= req.body;
        let username = req.header.username|| req.header('username');
        console.log(resps + username)

        let resp = new response({
            'username':username,
            'responses':resps
        });

        resp.save((err,ques)=>{
            if(err){
                return res.status(400).json({success:false,status:'Error!! Please Try Again',error:err.toString()});
            } else if(ques){
                return res.status(200).json({success:true,status:'response added successfully'});
            }else{
                return res.status(400).json({success:false,status:'Error!! Please Try Again',error:"Question not saved to db"});
            }
        });

    },
    deleteResponse(req,res){
        let username = req.header.username|| req.header('username');
        let id=req.body.id;
        user.findOne({'username':username},(err,user)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }else if(user){
                if(user.role==='admin'){
                    response.remove({'_id':id},(err,data)=>{
                        if(err){
                            res.status(400).json({success:false,status:'all question deletion failed'});
                        }
                        else{
                            res.status(200).json({success:true,status:'Question deleted'});
                        }
                    })
                }else{
                    res.status(200).json({success:false,status:"Insufficient permissions"})
                }
            }
        });
    },
    getResponse(req,res){
        let username = req.header.username|| req.header('username');
        response.find({'username':username},(err,respo)=>{
            if(err){
                return res.status(400).json({success:false,status:'Error!! Please Try Again'});
            }else if(respo){
                if(respo.responses !== null && respo.length>0){
                    return res.status(200).json({success:true,status:'Successful',response:respo});
                }else{

                    return res.status(200).json({success:false,status:'UnSuccessful',response:respo});
                }
            }else{
                return res.status(400).json({success:false,status:'Error!! Please Try Again'});
            }
        });
    },
    logout(req,res){
        let username = req.header.username|| req.header('username');
        mysession.remove({'username':username},(err,ques)=>{
            if(err){
                return res.status(400).json({success:false,status:'Error!! Please Try Again'});
            }else if(ques){
                return res.status(200).json({success:true,status:'Successful'});
            }else{
                return res.status(400).json({success:false,status:'Error!! Please Try Again'});
            }
        });
    },
    responses(req,res){
        let username = req.header.username|| req.header('username');
        user.findOne({'username':username},(err,usr)=>{
            if(err){

            }else if(usr){
                if(usr.role==='admin' || usr.role==='study'){
                    response.find({},(err,ques)=>{
                        if(err){
                            return res.status(400).json({success:false,status:'Error!! Please Try Again'});
                        }else if(ques){
                            return res.status(200).json({success:true,status:'Successful',questions:ques});
                        }else{
                            return res.status(400).json({success:false,status:'Error!! Please Try Again'});
                        }
                    });
                }else{
                    return res.status(200).json({success:false,status:'Onlu admin can add questions',error:"Question not saved to db"});
                }
            }
        });
    },
    addUser(req,res){
        let username = req.header.username|| req.header('username');
        let name = req.body.name;
        let uname = req.body.username;
        let password = req.body.password;



        user.findOne({'username':username},(err,User)=>{
            if(err){
                return res.status(400).json({success:false,status:'Error!! Please Try Again',error:err.toString()});
            }else if(User){
                if(User.role==='admin'){
                    let newUser = new user({
                        'name': name,
                        'username': uname,
                        'password': password,
                        'role':'user'
                    });

                    newUser.save((err,u)=>{
                        console.log(err);
                        if(err)
                            return res.status(400).json({success:false,status:'User already exists or you have entered invalid data'});
                        else
                            return res.status(200).json({success:true,status:'Registration Successful'});
                    });
                }else{
                    return res.status(200).json({success:false,status:'Only admin can add users'});
                }
            }
        });
    }
};

export default handle;
