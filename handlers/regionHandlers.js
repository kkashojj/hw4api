import discountData from '../static/SupportFiles/discount.json';
import config from '../config';
import fs from 'fs';
import path from 'path';
import product from '../model/product';

const regionFilter = (region)=>{
  if(region < config.regionList.length)
    return discountData.results.filter((item)=>{
      return item.region == config.regionList[region];
    })
  else {
    return "";
  }
}

const productFinder = (req,res)=>{
  if(req.query){
    let region = req.query.region;
    if(region!=0){
      product.find({'region':config.regionList[region]},(err,products)=>{
        if(err)
        console.log(err);
        else {
            res.send(products);
        }
      })
    }
    else {
      product.find({'region':{$ne : null}},(err,products)=>{
        if(err)
        console.log(err);
        else {
            res.send(products);
        }
      })
    }
  }
  else
    res.send("dfsndkln");
}

const imageFinder = (req,res)=>{
  if(req.query){
    let product = req.query.path;
    fs.readFile(path.join(config.staticImagePath,product),(err,data)=>{
      if(err){
        res.send("error in receiving data" + err);
      }
      else{
        res.set('Content-Type', 'image/jpeg');
        res.send(data);
      }
    })
  }

}

const productLoad= (req,res)=>{
   //dont let it work
  discountData.results.map((item)=>{
    let newProduct = new product({
      'discount': item.discount,
      'name': item.name,
      'photo': item.photo,
      'price': item.price,
      'region': item.region
    })
    console.log(item);
    newProduct.save((err,product)=>{
      if(err)
        console.log(err);
        else {
          console.log(product);
        }
    })
  })
}

const allRegions= (req,res)=>{
  res.status(200).send(["Region 1","Region 2","Region 3"]);
}

export default {productFinder,imageFinder,productLoad,allRegions}
