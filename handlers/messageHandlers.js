import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import config from '../config';
import message from '../model/message';
let uuid= require('uuid/v4');


const allMessages =(req,res)=>{
  message.find({'receiver': req.query.receiver},(err,messages)=>{   //only return 45 chars of message  path param
    if(err){
      res.status(200).json({success:false,message:"message retreival failed"})
    }
    else if(messages){
      res.status(200).send(messages);
    }
    else{
      res.status(200).json({success:false,message:"no messages"})
    }
  })
};

const deleteMessage = (req,res)=>{
  message.findOneAndRemove({'mid': req.params.mid},(err,msg)=>{
    if(err){
      res.status(200).json({success:false,message:"deletion failed"})
    }
    else{
      res.status(200).json({success:true,message:"deleted"})
    }
  })
};

const selectedMessage = (req,res)=>{
  message.findOne({'mid':req.params.mid},(err,msg)=>{
    if(err){
      res.status(200).json({success:false,message:"message retreival failed"})
    }
    else if(msg){
      res.status(200).send(msg);
    }
    else{
      res.status(200).json({success:false,message:"message doesnt exist"})
    }
  })
};

const updateMessage = (req,res)=>{ //how to update
  let setting;
  if(req.query.rstatus)
    setting = {'rstatus':true}
  else if(req.query.lstatus)
    setting = {'lstatus':true}
  else {
    res.status(200).send("invalid update");
  }
  message.findOneAndUpdate({'mid':req.params.mid},{$set:setting},(err,msg)=>{
    console.log(err);
    console.log(msg);
    if(err){
      res.status(200).json({success:false,message:"updation failed"})
    }
    else if(msg){
      res.status(200).json({success:true,message:"updation sucess"})
    }
    else{
      res.status(200).json({success:false,message:"updation failed"})
    }
  });
};

// const sendMessage = (req,res)=>{
//   //body of req
//   var data = req.body.message;
//   var msg = new message({
//     "mid": uuid(),
//     "sender": data.sender,
//     "receiver":data.receiver,
//     "desc":data.desc});
// res.send(msg);
// }

const sendMessage = (req,res)=>{
  //body of req
  var data = req.body.message;
  var msg = new message({
    "mid": uuid(),
    "sender": req.body.sender,
    "receiver":req.body.receiver,
    "region": req.body.region,
    "desc":req.body.data});
  msg.save((err,mesg)=>{
    if(err){
      res.status(200).json({success:false,message:"message send failed"})
    }
    else{
      res.status(200).json({success:true,message:"message send sucess"})
    }
  })
};

export default {allMessages,sendMessage,selectedMessage,deleteMessage,updateMessage}
